var express = require('express');
var router = express.Router();
//Medico
var medicoControl = require('../controladores/MedicoControl');
var medico = new medicoControl();
//Cuenta
var cuentaControl = require('../controladores/CuentaControl');
var cuenta = new cuentaControl();
//Paciente
var pacienteControl = require('../controladores/PacienteControl');
var paciente = new pacienteControl();
//Consulta
var consultaControl = require('../controladores/ConsultaControl');
var consulta = new consultaControl();

//MIDDLEWARE
function verificar_sesion(req) {
    return (req.session !== undefined && req.session.cuenta !== undefined);
}

var auth = function (req, res, next) {
    if (verificar_sesion(req)) {
        next();
    } else {
        req.flash('error', 'Se require que inicio de sesion!');
        res.redirect('/');
    }
}

/* GET home page. */
router.get('/', function (req, res, next) {
    if (req.session !== undefined && req.session.cuenta !== undefined) {
        res.render('index', {title: "Principal",
            fragmento: 'fragmentos/principal/frm_principal', sesion: true,
            usuario: req.session.cuenta.usuario});
    } else {
        res.render('index', {title: 'Medicos', msg: {error: req.flash('error'),
                ok: req.flash('success')}});
    }
    console.log(req.session.cuenta.usuario);
});


//Registro Medico
router.get('/registro_medico', medico.ver_registro);
router.post('/registro_medico', medico.guardar);
//Sesion-Cuenta
router.post('/inicio_sesion', cuenta.iniciar_sesion);
router.get('/cerrar_sesion', auth, cuenta.cerrar_sesion);
//Paciente
router.get('/administracion/pacientes', paciente.visualizar);
router.post('/administrador/pacientes/guardar', paciente.guardar);

router.get('/administracion/pacientes/modificar/:external', paciente.visualizarModificar);
router.post('/administracion/pacientes/modificar/update', paciente.modificar);
//Consulta
router.get('/administracion/consultas', consulta.visualizar);
router.post('/administracion/consultas/guardar', consulta.guardar);

router.get('/administracion/consulta/modificar/:external', consulta.visualizarModificar);
router.post('/administracion/consulta/modificar/update', consulta.modificar);

module.exports = router;
