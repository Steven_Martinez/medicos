'use strict'
var medico = require('../modelos/medico');
var cuenta = require('../modelos/cuenta');

class MedicoControl {
    ver_registro(req, res) {
        res.render('index', {
            title: 'Registrate',
            sesion: true,
            fragmento: 'fragmentos/medico/frm_registro_medico'});
    }

    guardar(req, res) {
        var dataM = {
            cedula: req.body.cedula,
            apellidos: req.body.apellidos,
            nombres: req.body.nombres,
            fecha_nac: req.body.fecha_nac,
            edad: req.body.edad,
            nro_registro: req.body.nro,
            especialidad: req.body.especialidad
        };
        var dataC = {
            correo: req.body.correo,
            clave: req.body.clave
        };
        var Medico = new medico(dataM);
        var Cuenta = new cuenta(dataC);
        Medico.cuenta = Cuenta;
        Medico.saveAll({cuenta: true}).then(function (registro) {
            //res.send(registro); 
            req.flash('success', 'Se registro correctamento');
            res.redirect('/');
        }).error(function (error) {
            console.log(error);
            res.send(error);
        });
    }
}

module.exports = MedicoControl;

