'use strict';
var pacienteC = require('../modelos/paciente');
var historiaC = require('../modelos/historia');
class PacienteControl {
    visualizar(req, res) {
        pacienteC.getJoin({historia: true}).then(function (todos) {
            var nro = "NHIS-" + (todos.length + 1);
            var m = req.session.cuenta.usuario;
            console.log("Medico en pacientes: " + m);
            res.render('index',
                    {title: 'Pacientes',
                        fragmento: "fragmentos/paciente/frm_tabla_paciente",
                        sesion: true,
                        listado: todos,
                        nro: nro,
                        msg: {error: req.flash('error'), info: req.flash('info')}
                    });
        }).error(function (error) {
            req.flash('error', 'Hubo un error!');
            res.redirect('/');
        });
    }

    guardar(req, res) {
        var dataP = {
            cedula: req.body.cedula,
            apellidos: req.body.apellidos,
            nombres: req.body.nombres,
            fecha_nac: req.body.fecha_nac,
            edad: req.body.edad,
            direccion: req.body.direccion
        };
        var paciente = new pacienteC(dataP);
        var dataH = {
            nro_historia: req.body.nro_his,
            enfermedades: req.body.enf,
            enfer_hede: req.body.enf_her,
            habitos: req.body.hab,
            contacto: req.body.contacto
        };
        var historia = new historiaC(dataH);
        paciente.historia = historia;
        paciente.saveAll({historia: true}).then(function (nuevo) {
            req.flash('info', 'Paciente registrado!');
            res.redirect('/administracion/pacientes');
        }).error(function (error) {
            req.flash('error', 'No se pudo registrar!');
            res.redirect('/administracion/pacientes');
        });
    }

    visualizarModificar(req, res) {
        var external = req.params.external;
        pacienteC.getJoin({historia: true}).filter({external_id: external}).then(function (data) {
            if (data.length > 0) {
                var paciente = data[0];
                res.render('index',
                        {title: 'Pacientes',
                            fragmento: "fragmentos/paciente/frm_modificar-paciente",
                            sesion: true,
                            paci: paciente,
                            msg: {error: req.flash('error'), info: req.flash('info')}
                        });
            } else {
                req.flash('error', 'No se pudo encontrar lo solicitado');
                res.redirect('/administracion/pacientes');
            }
        }).error(function (error) {

        });
    }

    modificar(req, res) {
        pacienteC.getJoin({historia: true}).filter({external_id: req.body.external}).then(function (data) {
            if (data.length > 0) {
                var paciente = data[0];
                //Data paciente
                paciente.apellidos = req.body.apellidos;
                paciente.nombres = req.body.nombres;
                paciente.edad = req.body.edad;
                paciente.direccion = req.body.direccion;
                paciente.fecha_nac = req.body.fecha_nac;
                //Data historia
                paciente.historia.enfermedades = req.body.enf;
                paciente.historia.enfer_hede = req.body.enf_her;
                paciente.historia.habitos = req.body.hab;
                paciente.historia.contacto = req.body.contacto;
                //Metodo para guardar las modificaciones
                paciente.saveAll({historia: true}).then(function (save) {
                    req.flash('info', 'Se ha modificado!');
                    res.redirect('/administracion/pacientes');
                }).error(function (error) {
                    req.flash('error', 'No se pudo Modificar!');
                    res.redirect('/administracion/pacientes');
                });
            } else {
                req.flash('error', 'No se pudo encontrar lo solicitado');
                res.redirect('/administracion/pacientes');
            }
        }).error(function (error) {

        });
    }

}
module.exports = PacienteControl;

