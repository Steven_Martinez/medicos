'use strict'
var consultaC = require('../modelos/consulta');
var pacienteC = require('../modelos/paciente');

class ConsultaControl {

    visualizar(req, res) {
        consultaC.getJoin({medico: true, historia: true}).then(function (todos) {
            //console.log(todos);
            pacienteC.getJoin({historia: true}).then(function (lista) {
                // console.log(lista);
                res.render('index',
                        {title: 'Consulta',
                            fragmento: "fragmentos/consulta/frm_consulta",
                            sesion: true,
                            listado: lista,
                            consulta: todos,
                            medico: req.session.cuenta,
                            msg: {error: req.flash('error'), info: req.flash('info')}
                        });
            }).error(function (error) {
                console.log("aqui 1");
                req.flash('error', 'Hubo un error al traer pacientes!');
                res.redirect('/administracion/consultas');
            });
        }).error(function (error) {
            console.log("aqui 2");
            req.flash('error', 'Hubo un error!');
            res.redirect('/administracion/consultas');
        });
    }

    guardar(req, res) {
        var medico = require('../modelos/medico');
        medico.filter({external_id: req.body.medico}).then(function (medico) {
            if (medico.length > 0) {
                var medicoC = medico[0];
                //console.log('Id_medico: ' + medicoC.id);
                var dataC = {
                    diagnostico: req.body.diagnostico,
                    fecha: req.body.fecha,
                    motivo: req.body.motivo,
                    receta: req.body.receta,
                    id_medico: medicoC.id,
                    id_historia: req.body.historia
                };
                var consulta = new consultaC(dataC);
                consulta.save().then(function (save) {
                    req.flash('info', 'Consulta registrada!');
                    res.redirect('/administracion/consultas');
                }).error(function (error) {
                    console.log(error);
                    req.flash('error', 'No se registro la consulta!');
                    res.redirect('/administracion/consultas');
                });
            } else {
                req.flash('erro', 'No encontra medicos');
                res.redirect('/administracion/consultas');
            }
        }).error(function (error) {
            console.log(error);
        });
    }

    visualizarModificar(req, res) {
        var external = req.params.external;
        consultaC.getJoin({medico: true}).filter({external_id: external}).then(function (data) {
            if (data.length > 0) {
                var consulta = data[0];
                //console.log("Consulta: " + consulta.id_historia);
                res.render('index',
                        {title: 'Modificar Consulta',
                            fragmento: "fragmentos/consulta/frm_modificar_consulta",
                            sesion: true,
                            consulta: consulta,
                            msg: {error: req.flash('error'), info: req.flash('info')}
                        });
                console.log("aqui 0");
            } else {
                req.flash('error', 'No se pudo encontrar lo solicitado');
                res.redirect('/administracion/consultas');
            }
        }).error(function (error) {

        });
    }

    modificar(req, res) {
        consultaC.getJoin({historia: true}).filter({external_id: req.body.external}).then(function (data) {
            if (data.length > 0) {
                var consulta = data[0];
                //Data paciente
                consulta.motivo = req.body.motivo;
                consulta.receta = req.body.receta;
                consulta.fecha = req.body.fecha;
                consulta.diagnostico = req.body.diagnostico;
                consulta.id_medico = req.body.medico;
                consulta.id_historia = req.body.historia;
                consulta.save().then(function (save) {
                    req.flash('info', 'Se ha modificado!');
                    res.redirect('/administracion/consultas');
                }).error(function (error) {
                    req.flash('error', 'No se pudo Modificar!');
                    res.redirect('/administracion/consultas');
                });
            } else {
                req.flash('error', 'No se pudo encontrar lo solicitado');
                res.redirect('/administracion/consultas');
            }
        }).error(function (error) {

        });
    }
}

module.exports = ConsultaControl;
