'use strict'
var cuentaC = require('../modelos/cuenta');

class CuentaControl {
    iniciar_sesion(req, res) {
        cuentaC.getJoin({medico: true}).filter({correo: req.body.correo}).run().then(function (account) {
            if (account.length > 0) {
                var cuenta = account[0];
                console.log(cuenta.medico.external_id);
                if (cuenta.clave === req.body.clave) {
                    req.session.cuenta = {
                        external: cuenta.medico.external_id,
                        usuario: cuenta.medico.apellidos + " " + cuenta.medico.nombres
                    };
                    console.log(req.session.cuenta.external);
                    res.redirect('/');
                } else {
                    req.flash('error', 'Sus credenciales no son las correctas!');
                    res.redirect('/');
                }
            } else {
                req.flash('error', 'Sus credenciales no son las correctas!');
                res.redirect('/');
            }
        }).error(function (error) {
            console.log(error);
        });
    }
    

    cerrar_sesion(req, res) {
            req.session.destroy();
            res.redirect('/');
    }

}
module.exports = CuentaControl;


