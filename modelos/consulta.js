var thinky = require('../config/thinky_init');
var type = thinky.type;
var r = thinky.r;
var Consulta = thinky.createModel("Consulta", {
    id: type.string(),
    external_id: type.string().default(r.uuid()),
    diagnostico: type.string(),
    fecha: type.date(),
    motivo: type.string(),
    receta: type.string(),
    createdAt: type.date().default(r.now()),
    updateAt: type.date().default(r.now()),
    id_medico: type.string(),
    id_historia: type.string()
});
module.exports = Consulta;
//Medico
var Medico = require('./medico');
Consulta.belongsTo(Medico, "medico", "id_medico", "id");
//Historia
var Historia = require('./historia');
Consulta.belongsTo(Historia, "historia", "id_historia", "id");

